#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#ifdef __MACH__
#include <mach/mach.h>
#include <mach/mach_time.h>

uint64_t hr_start() {
  return mach_absolute_time();
}

uint64_t hr_end(uint64_t hrstart) {
  uint64_t hrend = mach_absolute_time() - hrstart;

  mach_timebase_info_data_t sTimebaseInfo;
  mach_timebase_info(&sTimebaseInfo);

  return hrend * sTimebaseInfo.numer / sTimebaseInfo.denom;
}

#else

uint64_t hr_start() {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (uint64_t)ts.tv_nsec;
}

uint64_t hr_end(uint64_t hrstart) {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (uint64_t)ts.tv_nsec - hrstart;
}

#endif

#define METHOD_GET 0
#define METHOD_POST 1
#define METHOD_TYPES METHOD_POST+1

typedef void (*ReqHandler)();

void fakeGet() {
  //printf("faked get request called\n");
}
void fakePost() {
  //printf("faked post request called\n");
}

struct Node {
  struct Node* parent;
  size_t strLen;
  char* str;
  struct Node** children;
  ReqHandler* handlers;

  unsigned int childCount;
};

struct Trie {
  struct Node* root;
};

void freeNode(struct Node* n) {
  free(n->handlers);
  if (n->children) {
    for (int i = 0; i < n->childCount; i++) {
      freeNode(n->children[i]);
      free(n->children[i]);
      n->children[i] = NULL;
    }
    free(n->children);
  }
  free(n->str);
  n->parent = NULL;
}

void freeTrie(struct Trie* t) {
  freeNode(t->root);
  free(t->root);
  t->root = NULL;
}

struct Trie* readTrie() {
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  fp = fopen("trie-routes.txt", "r");
  if (fp == NULL) {
    return NULL;
  }

  struct Trie* t = malloc(sizeof(*t));
  t->root = NULL;

  struct Node* curNode = NULL;
  struct Node* lastNode = NULL;

  ssize_t indent = 0;
  size_t splitLoc = 0;
  size_t si = 0;
  size_t curLevel = 0;
  size_t lastLevel = 0;
  size_t urlLength;
  char* url;

  int needsGet = 0;
  int needsPost = 0;

  while ((read = getline(&line, &len, fp)) != -1) {
    //printf("Retrieved line of length %zu :\n", read);
    //printf("%s", line);

    indent = -1;
    splitLoc = (size_t)read;
    needsGet = 0;
    needsPost = 0;
    // TODO: optimize with bad ass pointer arithmatic
    for (si = 0; si < (size_t)read; si++) {
      if (indent == -1 && line[si] != ' ') {
        indent = si;
      }

      if (splitLoc) {
        if (line[si] == 'g') {
          needsGet = 1;
        }
        if (line[si] == 'p') {
          needsPost = 1;
        }
      }

      if (line[si] == '|') {
        splitLoc = si;
      }
    }

    curLevel = indent >> 1;

    urlLength = splitLoc - indent;
    if (splitLoc == (size_t)read) {
      // ignore the \n
      urlLength--;
    }

    url = malloc((urlLength + 1) * sizeof(char));
    strncpy(url, line+indent, urlLength);
    url[urlLength] = '\0';

    if (curLevel < lastLevel) {
      while (curLevel < lastLevel) {
        curNode = curNode ? curNode->parent : NULL;
        lastLevel--;
      }
    } else if (curLevel > lastLevel) {
      curNode = lastNode;
    }

    lastLevel = curLevel;

    //printf("create node for '%s'\n", url);

    lastNode = malloc(sizeof(*lastNode));
    lastNode->parent = curNode;
    lastNode->str = url;
    lastNode->strLen = strlen(url);
    lastNode->children = malloc(32 * sizeof(*lastNode)); // TODO: oh nooo, vectors? BST?
    lastNode->handlers = malloc(METHOD_TYPES * sizeof(*lastNode->handlers));
    lastNode->handlers[METHOD_GET] = needsGet ? fakeGet : NULL;
    lastNode->handlers[METHOD_POST] = needsPost ? fakePost : NULL;
    lastNode->childCount = 0;

    if (!curNode) {
      if (t->root) {
        printf("WARNING: the trie already had a root! bailing out\n");
        freeNode(lastNode);
        freeTrie(t);
        return NULL;
      }
      t->root = lastNode;
    } else {
      curNode->children[curNode->childCount] = lastNode;
      curNode->childCount++;
    }
  }

  free(line);
  line = NULL;

  fclose(fp);

  return t;
}

struct Node* binarySearch(struct Node* n, char ch) {
  int lb = 0;
  int ub = n->childCount - 1;
  int mid = 0;
  struct Node** cArr = n->children;
  struct Node* node = NULL;
  int d = 0;
  char diff = 0;

  while(1) {
    d = ub - lb;
    if (d < 0) {
      break;
    }
    mid = (d >> 1) + lb;
    node = cArr[mid];
    diff = node->str[0] - ch;

    if (diff == 0) {
      return node;
    } else if (diff < 0) {
      lb = mid + 1;
    } else {
      ub = mid - 1;
    }
  }

  return NULL;
}

ReqHandler searchTrie(struct Trie* t, const char* searchFor, int method) {
  size_t strLength = strlen(searchFor);

  if (strLength <= 1) {
    return t->root->handlers[method];
  }

  // skip the first /
  const char* ptr = searchFor + 1;
  strLength--;
  struct Node* curNode = t->root;

  size_t matchStrLen = 0;

  while(curNode && strLength) {
    curNode = binarySearch(curNode, ptr[0]);

    if (!curNode) {
      //printf("404 node found (no first str match)\n");
      break;
    }

    matchStrLen = curNode->strLen;

    if (matchStrLen > strLength || strncmp(ptr, curNode->str, matchStrLen)) {
      //printf("404 not found (not full match)\n");
      break;
    }

    ptr += matchStrLen;
    strLength -= matchStrLen;

    if (strLength) {
      if (!curNode->childCount) {
        //printf("404 not found (no more routes)\n");
        break;
      }
    } else {
      return curNode->handlers[method];
    }
  }

  return NULL;
}

void stressTestUrl(struct Trie* trie, const char* route, int method) {
  uint64_t hrstart;
  uint64_t ns;

  uint64_t min = 9999999;
  uint64_t max = 0;
  uint64_t total = 0;
  uint64_t avg = 0;

  int itersb2 = 8;
  int iters = (1 << itersb2);

  ReqHandler handler = NULL;

  for (int i=0; i < iters; i++) {
    hrstart = hr_start();
    handler = searchTrie(trie, route, method);
    ns = hr_end(hrstart);
    if (handler) {
      handler();
    }

    if (ns < min) {
      min = ns;
    }
    if (ns > max) {
      max = ns;
    }
    total += ns;
  }

  avg = total >> itersb2;

  printf("min: %"PRIu64" ns\n", min);
  printf("max: %"PRIu64" ns\n", max);
  printf("avg: %"PRIu64" ns\n\n", avg);

}

char* getNodeUrl(struct Node* node, char *buf) {
  char *ptr = buf;
  if (node->parent) {
    ptr = getNodeUrl(node->parent, buf);
  }
  size_t len = strlen(node->str);
  strncpy(ptr, node->str, len);
  ptr[len] = '\0';
  return ptr+len;
}

void printWalkedUrl(struct Trie* trie, struct Node* node) {
  char buffer[256];
  getNodeUrl(node, buffer);

  if (node->handlers) {
    if (node->handlers[METHOD_GET]) {
      printf("get %s\n", buffer);
    }
    if (node->handlers[METHOD_POST]) {
      printf("post %s\n", buffer);
    }
  }
}

void testWalkedUrl(struct Trie* trie, struct Node* node) {
  char buffer[256];

  getNodeUrl(node, buffer);

  if (node->handlers) {
    if (node->handlers[METHOD_GET]) {
      printf("GET '%s'\n", buffer);
      stressTestUrl(trie, buffer, METHOD_GET);
    }
    if (node->handlers[METHOD_POST]) {
      printf("POST '%s'\n", buffer);
      stressTestUrl(trie, buffer, METHOD_POST);
    }
  }
}

typedef void (*WalkerFunc)(struct Trie* trie, struct Node* node);
void trieWalker(struct Trie* trie, struct Node* node, WalkerFunc fn) {
  if (!node) {
    node = trie->root;
  }

  fn(trie, node);

  for (int i=0; i < node->childCount; i++) {
    trieWalker(trie, node->children[i], fn);
  }
}

int main(int argc, char** arv) {
  struct Trie* trie = readTrie();
  if (!trie) {
    return 1;
  }

  // warm up phase
  for (int i=0; i < 5; i++) {
    searchTrie(trie, "/", METHOD_GET);
  }

  trieWalker(trie, NULL, testWalkedUrl);

  ReqHandler handler = NULL;

  stressTestUrl(trie, "/verify", METHOD_GET);

  freeTrie(trie);
  return 0;
}
