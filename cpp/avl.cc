#include "avl.h"

#include <string.h>

class AvlNode {
public:
  AvlNode* _parent;
  AvlNode* _leftChild;
  AvlNode* _rightChild;

  RouteNode* _data;
  int _balance;

  AvlNode(AvlNode* parent, RouteNode* data) {
    _parent = parent;
    _data = data;
    _balance = 0;
    _leftChild = NULL;
    _rightChild = NULL;
  }

  ~AvlNode() {
    if (_leftChild) { delete _leftChild };
    if (_rightChild) { delete _rightChild };

    delete _data;
    _parent = NULL;
  }

  AvlNode* rotateLeft() {
    AvlNode* n = this->_rightChild;

    /* this is null when p is the root node */
    if (this->_parent) {
      if (this->_parent->leftChild == this) {
        this->_parent->leftChild = n;
      } else {
        this->_parent->rightChild = n;
      }
    }

    this->_rightChild = n->leftChild;
    if (this->_rightChild) {
      this->_rightChild->parent = this;
    }

    n->parent = this->_parent;
    this->_parent = n;
    n->leftChild = this;

    return n;
  }

  AvlNode* rotateRight() {
    AvlNode* n = this->_leftChild;

    if (this->_parent) {
      if (this->_parent->leftChild == this) {
        this->_parent->leftChild = n;
      } else {
        this->_parent->rightChild = n;
      }
    }

    this->_leftChild = n->rightChild;
    if (this->_leftChild) {
      this->_leftChild->parent = this;
    }

    n->parent = this->_parent;
    this->_parent = n;
    n->rightChild = this;

    return n;
  }
};


RouteNode::RouteNode(std::string str, Local<RegExp> regex) {
  _str = str;
  // TODO: is there like a retain we need to do?
  _regex = regex;
  _chCode = str.length() ? str[0] : 0;
  _rootChild = NULL;
  _dynamicPath = new std::vector<RouteNode>();
  _params = new std::vector<RouteNode>();
}

RouteNode::~RouteNode() {
  if (rootChild) { delete rootChild };
}

void RouteNode::addHandler(std::vector<int> methods, Local<Function> handler) {
  for (int i=0; i < methods.size(); i++) {
    if (!_handler[methods[i]]) {
      // TODO: any retain?
      _handler[methods[i]] = handler;
    }
  }
}

void RouteNode::insertChild(RouteNode* newNode) {
  AvlNode* p = NULL;
  AvlNode* n = _rootChild;
  char diff = 0;
  char ch = newNode->_chCode;

  while(n) {
    diff = n->data->_chCode - ch;

    if (diff == 0) {
      // TODO: how to notify the user Hey, this is a duplicate.
      return;
    } else if (diff < 0) {
      p = n;
      n = n->rightChild;
    } else {
      p = n;
      n = n->leftChild;
    }
  }

  // n will be NULL and p will be the parent for the new node
  n = new AvlNode(p, newNode);
  if (!_rootChild) {
    _rootChild = n;
    return;
  }

  if (diff < 0) {
    p->rightChild = n;
  } else {
    p->leftChild = n;
  }

  do {
    if (n == p->leftChild) {
      if (p->balance == 1) {
        if (n->balance == -1) {
           rotateLeft(n);
        }
        AvlNode* newP = rotateRight(p);
        // TODO: is this supposed to be no-parent ?
        if (p->parent) {
          _rootChild = newP;
        }
        break;
      }
      if (p->balance == -1) {
        p->balance = 0;
        break;
      }
      p->balance = 1;
    } else {
      if (p->balance == -1) {
        if (n->balance == 1) {
          rotateRight(n);
        }
        AvlNode* newP = rotateLeft(p);
        if (p->parent) {
          _rootChild = newP;
        }
        break;
      }
      if (p->balance == 1) {
        p->balance = 0;
        break;
      }
      p->balance = -1;
    }
    n = p;
    p = n->parent;
  } while (p);
}

void RouteNode::insertDynamicNode(RouteNode* node) {
  // TODO: regex sorting
  _dynamicPath.push(node);
}

RouteNode* RouteNode::binarySearch(char ch) {
  AvlNode* n = _rootChild;
  char diff = 0;

  while(n) {
    diff = n->data->str[0] - ch;

    if (diff == 0) {
      return n->data;
    } else if (diff < 0) {
      n = n->rightChild;
    } else {
      n = n->leftChild;
    }
  }

  return NULL;
}
