#ifndef AVL_H
#define AVL_H

#include <node.h>
#include <string>
#include <vector>

#define METHOD_GET 0
#define METHOD_POST 1
#define METHOD_TYPES 2

using v8::Local;
using v8::Function;
using v8::String;
using v8::RegExp;

class AvlNode;

// TODO: optimize structure ordering by byte-length
class RouteNode {
public:
  std::string _str;
  Local<RegExp> _regex;
  char _chCode;
  AvlNode* _rootChild;
  std::vector<AvlNode> _dynamicPath;
  std::vector<AvlNode> _params;
  Local<Function> _handler[METHOD_TYPES];

  RouteNode(std::string str, Local<RegExp> regex);
  ~RouteNode();

  void addHandler(vector<int> methods, Local<Function> handler);
  void insertChild(RouteNode* node);
  void insertDynamicNode(RouteNode* node);

  RouteNode* binarySearch(char ch);
};

#endif
