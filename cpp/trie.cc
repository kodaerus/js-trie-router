#include "trie.h"

#include <string.h>

struct AddFindNodeResult {
  /* depending on isPartial, may be the parent node for inserting or a node which needs to be split */
  RouteNode *node;

  /*
   * the remaining string that was not matched
   * it should not be NULL if isPartialMatch=1. you matched it all. Move on with your life
   */
  const char *remaining;

  /* 0: just insert there, 1: the node must be split aka partial node-match */
  int isPartialMatch;
};

struct HandleFindNodeResult {
  RouteNode *node;
  std::vector<std::string> params;
};


RouteTrie::RouteTrie() {
  _rootNode = new RouteNode("/", NULL);
}

RouteTrie::~RouteTrie() {
  if (_rootNode) { delete _rootNode; }
}

AddFindNodeResult RouteTrie::findNodeForAdd(RouteNode *startNode, std::string path) {
  AddFindNodeResult result;
  result.node = NULL;
  result.remaining = NULL;
  result.isPartialMatch = 0;

  RouteNode* curNode = startNode;
  RouteNode* lastNode = NULL;

  size_t strLength = path.length();
  size_t rootLength = startNode->str.length();

  if (strLength == rootLength && path == startNode->str) {
    result.node = startNode;
    return result;
  }

  // TODO: um... using ptr arith for super speed...
  const char* ptr = path;
  size_t matchStrLen = 0;

  while(curNode && strLength) {
    lastNode = curNode;
    curNode = lastNode->binarySearch(ptr[0]);

    if (!curNode) {
      result.node = lastNode;
      result.remaining = ptr;
      break;
    }

    matchStrLen = curNode->strLen;

    if (matchStrLen > strLength || strncmp(ptr, curNode->str, matchStrLen)) {
      result.node = curNode;
      result.remaining = ptr; /* the other side will figure out how much to split by */
      result.isPartialMatch = 1;
      /* if not, we will be returning no match anyway */
      break;
    }

    ptr += matchStrLen;
    strLength -= matchStrLen;

    if (strLength) {
      if (!curNode->rootChild) {
        result.node = curNode;
        result.remaining = ptr;
        break;
      }
    } else {
      result.node = curNode;
      break;
    }
  }

  return result;
}

RouteNode* RouteTrie::addNodes(RouteNode *startNode, std::string path) {
  // get the parent node (or a node to split)
  AddFindNodeResult result = findNodeForAdd(startNode, path);
  RouteNode *newNode = NULL;

  if (!result.node) {
    // theoretically, this isn't possible if canBePartial is 1. we will at least get the root
    // don't really want a panic though
    // TODO: throw me
    return NULL;
  }

  if (result.remaining && *result.remaining) {
    // there be splittin that needs to happen
    if (result.isPartialMatch) {
      // has remaining and partial means either node.str is longer OR two strings start with some base string

      // TODO: more c vs cpp strings
      const char *p1, *p2;
      for (p1 = result.remaining, p2 = result.node->_str; *p1 && *p1 == *p2; p1++, p2++) {}

      // it's possible p1 could be at the end of path
      // p2 cannot be at the end of result.node.str
      // or p1 and p2 could just be in the middle of both strings

      /*
        we start by creating a new node to hold the remainder of result.node.str
        move the children array, handlers, etc over.

        modify the original node to contain only the shared, base string,
        start with fresh children array, no handlers, etc.
        add the new node as the first child.

        finally, if there's any p1 remaining, create a new node for it.
        else, just add the handler to the now-modified "original" node.
      */

      // TODO: where i left off: getting substring. also, the JS version passes false for regex. that's wrong
      RouteNode *i2Node = new RouteNode(result.node->_str.substring(p2), NULL);
      i2Node->_rootNode = result.node->_rootNode;
      // TODO: array copy
      i2Node->_handlers = result.node->_handlers;

      result.node->_str = result.node->_str.substring(0, p2);
      result.node->_chCode = result.node->_str[0];
      result.node->_rootNode = NULL;
      result.node->insertChild(i2Node);
      // TODO: clear out handlers
      result.node->_handlers;

      if (!p1) {
        newNode = result.node;
      } else {
        // TODO: cstring ptr
        newNode = new RouteNode(path.substring(p1), NULL);
        result.node->insertChild(newNode);
      }
    } else {
      // TODO: cstring ptr
      newNode = new RouteNode(path.substring(result.remaining), NULL);
      result.node->insertChild(newNode);
    }
  } else {
    return result.node;
  }

  return newNode;
}

bool RouteTrie::addPath(std::vector<int> methods, std::string path, handler);
HandleFindNodeResult RouteTrie::matchPathToNode(std::string path, int method);

bool RouteTrie::add(Local<Object> config);
bool RouteTrie::handle(Local<Object> request, Local<Object> response);


/* if canBePartial is 0, then it must be an exact match.
 * FindNodeResult.isPartialMatch must be 0 in this case and FindNodeResult.remaining must be NULL / '\0'
 *
 * if canBePartial is 1, then returns the most similar node it can.
 * either the parent to insert under or a node which must be split
 */
struct FindNodeResult findNode(struct RouteTrie* trie, const char *url, int method, int canBePartial) {
  struct FindNodeResult result;
  result.node = NULL;
  result.remaining = NULL;
  result.isPartialMatch = 0;

  struct RouteNode* curNode = trie->methodTrieRoot[method];
  struct RouteNode* lastNode = NULL;

  size_t strLength = strlen(url);

  if (strLength == 0) {
    result.node = curNode;
    result.remaining = NULL;
    result.isPartialMatch = 0;
    return result;
  }

  // TODO: must a URL start with a slash?

  const char* ptr = url;
  size_t matchStrLen = 0;

  while(curNode && strLength) {
    lastNode = curNode;
    curNode = searchRouteNode(curNode, ptr[0]);

    if (!curNode) {
      if (canBePartial) {
        result.node = lastNode;
        result.remaining = ptr;
        result.isPartialMatch = 0;
      }
      break;
    }

    matchStrLen = curNode->strLen;

    if (matchStrLen > strLength || strncmp(ptr, curNode->str, matchStrLen)) {
      if (canBePartial) {
        result.node = curNode;
        result.remaining = ptr; /* the other side will figure out how much to split by */
        result.isPartialMatch = 1;
      }
      /* if not, we will be returning no match anyway */
      break;
    }

    ptr += matchStrLen;
    strLength -= matchStrLen;

    if (strLength) {
      if (!curNode->rootChild) {
        result.node = curNode;
        result.remaining = ptr;
        result.isPartialMatch = 0;
        break;
      }
    } else {
      result.node = curNode;
      result.remaining = NULL;
      result.isPartialMatch = 0;
      break;
    }
  }

  return result;
}

void createRouteNodes(struct RouteNode* parentNode, const char *url, ReqHandler handler) {
  if (!parentNode || !url || !handler) {
    // TODO: why would you even, man
    return;
  }

  // "split" by '/'. not really a true split, more like iterating up to '/' or '\0'
  // insert nodes, but also insert the '/' itself

  struct RouteNode* p = parentNode;
  struct RouteNode* newNode = NULL;

  const char *remaining = url;
  const char *slashPtr;
  char *copiedString;
  size_t substrLength;

  while (*remaining) {
    slashPtr = strchr(remaining, '/');

    if (slashPtr) {
      substrLength = slashPtr - remaining;
    } else {
      substrLength = strlen(remaining);
    }

    if (substrLength) {
      copiedString = malloc((substrLength+1) * sizeof(*copiedString));
      strncpy(copiedString, remaining, substrLength);
      copiedString[substrLength] = '\0';

      /* only set the handler at the very end */
      newNode = createRouteNode(p, copiedString, NULL);
      copiedString = NULL; /* we transfered ownership, don't remember our lost toys */
      insertRouteNode(p, newNode);
      p = newNode;

      remaining += substrLength;
    }

    if (slashPtr) {
      copiedString = malloc(2 * sizeof(*copiedString));
      copiedString[0] = '/';
      copiedString[1] = '\0';

      newNode = createRouteNode(p, copiedString, NULL);
      copiedString = NULL;
      insertRouteNode(p, newNode);
      p = newNode;

      remaining++;
    }
  }

  /* we ended with a slashPtr, so put the handler on it's parent */
  if (slashPtr) {
    if (!p->parent->handler) {
      p->parent->handler = handler;
    } else {
      // TODO: what to do with the case of existing handler?
      // the only way for it to happen is if the user adds a handler to '/myRoute' and '/myRoute/'
    }
  } else {
    p->handler = handler;
  }
}

int insertRoute(struct RouteTrie* trie, const char *url, int method, ReqHandler handler) {
  struct FindNodeResult result = findNode(trie, url, method, 1);

  if (!result.node) {
    // theoretically, this isn't possible if canBePartial is 1. we will at least get the root
    // don't really want a panic though
    return -1;
  }

  if (result.remaining && *result.remaining) {
    // there be splittin that needs to happen
    if (result.isPartialMatch) {
      // has remaining and partial means either our string is longer OR two strings are similar, yet not same
      // need to check if the ptr-diff is within node's str, if so we need to generate 2 new nodes with a split

      const char *p1, *p2;
      for (p1 = result.remaining, p2 = result.node->str; *p1 && *p1 != '/' && *p1 == *p2; p1++, p2++) {}

      if (*p2) {
        /*
          then we need to create a new pnode to have (p1-remaining),
          free node->str and make a new node->str starting at p2
          add that node to the pnode
          have createRouteNodes add the rest to pnode
        */
        size_t substrLength = p1 - result.remaining;
        /* the shared portion */
        char *sharedStr = malloc((substrLength+1) * sizeof(*sharedStr));
        strncpy(sharedStr, result.remaining, substrLength);
        sharedStr[substrLength] = '\0';

        /* the remaining chars of the old node */
        char *modNodeStr = malloc((strlen(p2)+1) * sizeof(*modNodeStr));
        strcpy(modNodeStr, p2);

        /* the new "old node" now split apart. we reuse result.node to not have to do any AVL-removals */
        struct RouteNode* modNode = createRouteNode(result.node, modNodeStr, result.node->handler);
        modNode->rootChild = result.node->rootChild;

        free(result.node->str);
        result.node->str = sharedStr;
        result.node->strLen = substrLength;
        result.node->rootChild = NULL;
        result.node->handler = NULL;

        insertRouteNode(result.node, modNode);

        /* add all the new nodes now that we've split */
        createRouteNodes(result.node, p1, handler); /* p1 is the remaining string */
      } else {
        /*
          TODO: how to handle case that can only be caused by a bug or weird edge I didn't think about?
          if *p2 was NULL, then findNode would have saw a match and kept going. this just shouldn't happen.
        */
      }
    } else {
      // have remaining yet no partial match is an easy just insert a child into the node
      createRouteNodes(result.node, result.remaining, handler);
    }
  } else {
    if (result.node->handler) {
      /*
        instead of overwriting, probably should complain to the user in some way
        isPartialMatch should be 0 too, no way for a partial match here
      */
      return 1;
    } else {
      // might have the node due to a sub-directory, but it has no handler. now it does.
      result.node->handler = handler;
    }
  }

  return 0;
}

ReqHandler findHandlerForRoute(struct RouteTrie* trie, const char *url, int method) {
  struct FindNodeResult result = findNode(trie, url, method, 0);

  if (result.node) {
    return result.node->handler;
  }

  return NULL;
}
