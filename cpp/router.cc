#include <stdio.h>
#include <stdlib.h>

#include "trie.h"

#include <time.h>
#include <sys/time.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#ifdef __MACH__
#include <mach/mach.h>
#include <mach/mach_time.h>

uint64_t hr_start() {
  return mach_absolute_time();
}

uint64_t hr_end(uint64_t hrstart) {
  uint64_t hrend = mach_absolute_time() - hrstart;

  mach_timebase_info_data_t sTimebaseInfo;
  mach_timebase_info(&sTimebaseInfo);

  return hrend * sTimebaseInfo.numer / sTimebaseInfo.denom;
}

#else

uint64_t hr_start() {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (uint64_t)ts.tv_nsec;
}

uint64_t hr_end(uint64_t hrstart) {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return (uint64_t)ts.tv_nsec - hrstart;
}

#endif

void fakeGet() {
  printf("faked get request called\n");
}
void fakePost() {
  printf("faked post request called\n");
}

int main(int argc, char** arv) {
  printf("Start testing\n");
  struct RouteTrie* trie = createRouteTrie();
  if (!trie) {
    printf("FAILED: could not create the RouteTrie\n");
    return 1;
  }
  printf("created RouteTrie\n");

  uint64_t hrstart;
  uint64_t ns;

  const char* routes[] = {
    // "/",
    // "/test",
    // "/test/hello",
    // "/about",
    // "/testing",
    // "/absolve"
    "/",
    "/about",
    "/account",
    "/account/cancelAvatar",
    "/account/changePass",
    "/account/company/acceptRequest/:requestId",
    "/account/company/cancelAvatar",
    "/account/company/cancelJoin",
    "/account/company/create",
    "/account/company/cropAvatar",
    "/account/company/delete",
    "/account/company/denyRequest/:requestId",
    // "/account/company/edit",
    "/account/company/edit",
    "/account/company/join",
    "/account/company/joinConfirm/:id",
    "/account/company/joinStatus",
    "/account/company/leave",
    "/account/company/manageMembers",
    "/account/cropAvatar",
    "/account/save",
    "/account/user",
    "/ackNotification",
    "/advertise",
    "/answer",
    "/answer/delete",
    "/answer/finishEdit",
    "/answer/startEdit",
    "/auth/account-recovery",
    // "/auth/account-recovery",
    "/auth/facebook",
    "/auth/facebook/callback",
    "/auth/google",
    "/auth/google/callback",
    "/auth/linkedin",
    "/auth/linkedin/callback",
    "/auth/reset/:resetToken",
    "/auth/resendVerification",
    "/auth/reset",
    "/auth/twitter",
    "/auth/twitter/callback",
    "/clarify",
    "/clarify-delete",
    "/comment",
    "/comment/delete",
    "/community",
    "/company",
    "/company/addAdmin",
    "/company/demoteUser",
    "/company/promoteUser",
    "/company/removeAdmin",
    "/company/removeUser",
    "/company/saveMemberOrder",
    "/company/top",
    "/company/:cid",
    "/company/:cid/answers",
    "/company/:cid/members",
    "/company/:cid/votes",
    "/companies",
    "/companies/top",
    // "/contact",
    "/contact",
    "/emailcheck",
    "/favorite/:qid",
    "/mobileask",
    "/mobilenotifications",
    "/mobilesearch",
    "/mobilesettings",
    "/notification",
    "/privacy",
    "/question",
    "/question/delete",
    "/question/finishEdit",
    "/question/lock",
    "/question/startEdit",
    "/question/:qid",
    "/question/:qid/:slug",
    "/questions",
    "/questions/top",
    "/register",
    "/report/answer",
    "/report/comment",
    "/report/company",
    "/report/question",
    "/report/user",
    "/search",
    // "/signin",
    "/signin",
    "/signinMobile",
    "/signout",
    "/signup",
    "/tag/:tagName",
    "/tags",
    "/terms",
    "/unfavorite/:qid",
    "/uploadDone",
    "/user",
    "/user/top",
    "/user/:uid",
    "/user/:uid/answers",
    "/user/:uid/favorites",
    "/user/:uid/lock",
    "/user/:uid/questions",
    "/user/:uid/unlock",
    "/user/:uid/votes",
    "/users",
    "/users/experts",
    "/users/top",
    "/verify",
    "/vote/:answer"
  };
  size_t numRoutes = sizeof(routes) / sizeof(routes[0]);

  int insertResult = 0;

  int i;
  for (i=0; i < numRoutes; i++) {
    hrstart = hr_start();
    insertResult = insertRoute(trie, routes[i], METHOD_GET, fakeGet);
    ns = hr_end(hrstart);
    if (insertResult) {
      printf("FAILED: could not add the route '%s'\n", routes[i]);
      freeRouteTrie(trie);
      return 1;
    }
    printf("inserted '%s' in %lu ns\n", routes[i], (unsigned long)ns);
  }

  // walkNode(trie->methodTrieRoot[METHOD_GET], 0);

  ReqHandler handler = NULL;
  for (i=0; i < numRoutes; i++) {
    hrstart = hr_start();
    handler = findHandlerForRoute(trie, routes[i], METHOD_GET);
    ns = hr_end(hrstart);
    if (handler) {
      printf("found handler for for '%s' in %lu ns\n", routes[i], (unsigned long)ns);
      handler();
    } else {
      printf("could not find the handler for '%s' in %lu ns\n", routes[i], (unsigned long)ns);
    }
  }

  handler = findHandlerForRoute(trie, "/notthere", METHOD_GET);
  if (handler) {
    printf("WARNING: '/notthere' should not exist!\n");
    handler();
  }

  freeRouteTrie(trie);
  printf("complete!\n");
  return 0;
}
