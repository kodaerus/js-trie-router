#ifndef TRIE_H
#define TRIE_H

#include "avl.h"
#include <node.h>
#include <string>
#include <vector>

using v8::Function;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Null;
using v8::Object;
using v8::String;
using v8::Value;

class RouteNode;

struct AddFindNodeResult;
struct HandleFindNodeResult;

class RouteTrie {
public:
  RouteNode *_rootNode;

  RouteTrie();
  ~RouteTrie();

private:
  AddFindNodeResult findNodeForAdd(RouteNode *startNode, std::string path);
  RouteNode* addNodes(RouteNode *startNode, std::string path);
  bool addPath(std::vector<int> methods, std::string path, handler);
  HandleFindNodeResult matchPathToNode(std::string path, int method);

public:
  bool add(Local<Object> config);
  bool handle(Local<Object> request, Local<Object> response);
};

#endif
