#include "avl.h"

#include <string.h>

struct AvlNode {
  struct AvlNode* parent;
  struct AvlNode* leftChild;
  struct AvlNode* rightChild;
  struct RouteNode* data;
  int balance;
};

struct AvlNode* createAvlNode(struct AvlNode* parent, struct RouteNode* data);
void freeAvlNode(struct AvlNode* avlNode);


struct RouteNode* createRouteNode(struct RouteNode* parent, const char* str, ReqHandler handler) {
  struct RouteNode* node = malloc(sizeof(*node));
  node->rootChild = NULL;
  node->parent = parent;
  node->str = str;
  node->strLen = strlen(str);
  node->handler = handler;
  return node;
}

struct AvlNode* createAvlNode(struct AvlNode* parent, struct RouteNode* data) {
  struct AvlNode* node = malloc(sizeof(*node));
  node->parent = parent;
  node->data = data;
  node->balance = 0;

  node->leftChild = NULL;
  node->rightChild = NULL;

  return node;
}

void freeRouteNode(struct RouteNode* node) {
  if (node) {
    freeAvlNode(node->rootChild);

    free(node->str);
    node->handler = NULL;
    node->parent = NULL;
    free(node);
  }
}

void freeAvlNode(struct AvlNode* node) {
  if (node) {
    freeAvlNode(node->leftChild);
    freeAvlNode(node->rightChild);

    freeRouteNode(node->data);
    node->parent = NULL;
    free(node);
  }
}

struct RouteNode* searchRouteNode(struct RouteNode* node, char ch) {
  struct AvlNode* n = node->rootChild;
  char diff = 0;

  while(n) {
    diff = n->data->str[0] - ch;

    if (diff == 0) {
      return n->data;
    } else if (diff < 0) {
      n = n->rightChild;
    } else {
      n = n->leftChild;
    }
  }

  return NULL;
}

struct AvlNode* rotateLeft(struct AvlNode* p) {
  struct AvlNode* n = p->rightChild;

  /* this is null when p is the root node */
  if (p->parent) {
    if (p->parent->leftChild == p) {
      p->parent->leftChild = n;
    } else {
      p->parent->rightChild = n;
    }
  }

  p->rightChild = n->leftChild;
  if (p->rightChild) {
    p->rightChild->parent = p;
  }

  n->parent = p->parent;
  p->parent = n;
  n->leftChild = p;

  return n;
}

struct AvlNode* rotateRight(struct AvlNode* p) {
  struct AvlNode* n = p->leftChild;

  if (p->parent) {
    if (p->parent->leftChild == p) {
      p->parent->leftChild = n;
    } else {
      p->parent->rightChild = n;
    }
  }

  p->leftChild = n->rightChild;
  if (p->leftChild) {
    p->leftChild->parent = p;
  }

  n->parent = p->parent;
  p->parent = n;
  n->rightChild = p;

  return n;
}

void insertRouteNode(struct RouteNode* parent, struct RouteNode* newNode) {
  struct AvlNode* p = NULL;
  struct AvlNode* n = parent->rootChild;
  char diff = 0;
  char ch = newNode->str[0];

  while(n) {
    diff = n->data->str[0] - ch;

    if (diff == 0) {
      // TODO: how to notify the user Hey, this is a duplicate.
      return;
    } else if (diff < 0) {
      p = n;
      n = n->rightChild;
    } else {
      p = n;
      n = n->leftChild;
    }
  }

  // n will be NULL and p will be the parent for the new node
  n = createAvlNode(p, newNode);
  if (!parent->rootChild) {
    parent->rootChild = n;
    return;
  }

  if (diff < 0) {
    p->rightChild = n;
  } else {
    p->leftChild = n;
  }

  do {
    if (n == p->leftChild) {
      if (p->balance == 1) {
        if (n->balance == -1) {
           rotateLeft(n);
        }
        struct AvlNode* newP = rotateRight(p);
        if (p->parent) {
          parent->rootChild = newP;
        }
        break;
      }
      if (p->balance == -1) {
        p->balance = 0;
        break;
      }
      p->balance = 1;
    } else {
      if (p->balance == -1) {
        if (n->balance == 1) {
          rotateRight(n);
        }
        struct AvlNode* newP = rotateLeft(p);
        if (p->parent) {
          parent->rootChild = newP;
        }
        break;
      }
      if (p->balance == 1) {
        p->balance = 0;
        break;
      }
      p->balance = -1;
    }
    n = p;
    p = n->parent;
  } while (p);
}

// void walkAvl(struct AvlNode* node, int indent) {
//   if (node->leftChild) {
//     walkAvl(node->leftChild, indent);
//   }
//
//   walkNode(node->data, indent);
//
//   if (node->rightChild) {
//     walkAvl(node->rightChild, indent);
//   }
// }
//
// void walkNode(struct RouteNode* node, int indent) {
//   int i;
//   for (i=0; i < indent; i++) {
//     printf(" ");
//   }
//   printf("%s\n", node->str);
//
//   if (node->rootChild) {
//     walkAvl(node->rootChild, indent+2);
//   }
// }
