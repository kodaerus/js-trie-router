#ifndef TRIE_H
#define TRIE_H

#include <stdlib.h>
#include "avl.h"

#define METHOD_GET 0
#define METHOD_POST 1
#define METHOD_TYPES 2

struct RouteNode;

struct RouteTrie {
  struct RouteNode *methodTrieRoot[METHOD_TYPES];
};

struct RouteTrie* createRouteTrie();
void freeRouteTrie(struct RouteTrie* trie);

int insertRoute(struct RouteTrie* trie, const char *url, int method, ReqHandler handler);

ReqHandler findHandlerForRoute(struct RouteTrie* trie, const char *url, int method);

#endif
