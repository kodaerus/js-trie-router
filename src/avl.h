#ifndef AVL_H
#define AVL_H

#include <stdlib.h>

// TODO: add some http context in here
typedef void (*ReqHandler)();

struct AvlNode;

struct RouteNode {
  struct RouteNode* parent;
  struct AvlNode* rootChild;

  const char* str;
  ReqHandler handler;

  size_t strLen;
};

struct RouteNode* createRouteNode(struct RouteNode* parent, const char* str, ReqHandler handler);
void freeRouteNode(struct RouteNode* avlNode);
struct RouteNode* searchRouteNode(struct RouteNode* node, char ch);
void insertRouteNode(struct RouteNode* node, struct RouteNode *newNode);

void walkNode(struct RouteNode* node, int indent);

#endif
