var mine = require('./js/router');
mine.add({
  methods: 'get',
  path: '/',
  handler: function(req, res) {console.log('/')},
  options: {}
});
mine.add({
  methods: 'get',
  path: '/hello',
  handler: function(req, res) {console.log('hello')}
});
mine.add({
  methods: 'get',
  path: '/hello/world',
  handler: function(req, res) {console.log('hello world')}
});
mine.add({
  methods: 'get',
  path: '/testing',
  handler: function(req, res) {console.log('testing')}
});
mine.add({
  methods: 'get',
  path: '/lets/see/how/great/this/is',
  handler: function(req, res) {console.log('murica is great')}
});
mine.add({
  methods: 'get',
  path: '/{pg}/world',
  handler: function(req, res) {console.log(req.params.pg, 'is a world')}
});
mine.add({
  methods: 'get',
  path: '/nv{nav(about)}',
  handler: function(req, res) {console.log('nvpg', req.params)},
  options: {}
});
mine.add({
  methods: 'get',
  path: '/{identity(page)}/{id([0-9a-f]{24}|\\w{3,30})}',
  handler: function(req, res) {console.log('pgshort')},
  options: {}
});
mine.add({
  methods: 'get',
  path: '/{identity(page)}/{id([0-9a-f]{24}|\\w{3,30})}/{nav(posts)}/{tab(new)}',
  handler: function(req, res) {console.log('pglong')},
  options: {}
});

//mine.print();

mine.handle('get', '/').route.handlers['get']({params: {}}, {});
mine.handle('get', '/hello').route.handlers['get']({params: {}}, {});
mine.handle('get', '/hello/world').route.handlers['get']({params: {}}, {});
mine.handle('get', '/testing').route.handlers['get']({params: {}}, {});
mine.handle('get', '/lets/see/how/great/this/is').route.handlers['get']({params: {}}, {});
mine.handle('get', '/earth/world').route.handlers['get']({params: {}}, {});
mine.handle('get', '/nvabout').route.handlers['get']({params: {}}, {});
mine.handle('get', '/page/taylorswift').route.handlers['get']({params: {}}, {});
mine.handle('get', '/page/taylorswift/posts/new').route.handlers['get']({params: {}}, {});

// 'use strict'
//
// var TrieRouter = require('./experimental-router');
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/',
//   handler: function(req, res) {
//     console.log('index');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/aboutus',
//   handler: function(req, res) {
//     console.log('aboutus');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/abo',
//   handler: function(req, res) {
//     console.log('abo');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/test',
//   handler: function(req, res) {
//     console.log('test');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/tester',
//   handler: function(req, res) {
//     console.log('tester');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/barn',
//   handler: function(req, res) {
//     console.log('barn');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/basket',
//   handler: function(req, res) {
//     console.log('basket');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/test/testing/the/testers',
//   handler: function(req, res) {
//     console.log('test super long one');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/test/testing/the/test',
//   handler: function(req, res) {
//     console.log('test super long one part 2 yet shorter');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/{var1}',
//   handler: function(req, res) {
//     console.log('dynamic path 1');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/{var1}/{var2}',
//   handler: function(req, res) {
//     console.log('dynamic path 2');
//   },
//   options: {}
// });
//
// TrieRouter.add({
//   methods: 'get',
//   path: '/{nav(about)}',
//   handler: function(req, res) {
//     console.log('about');
//   },
//   options: {}
// });
// TrieRouter.add({
//   methods: 'get',
//   path: '/{identity(page)}/{id([0-9a-f]{24}|\\w{3,30})}',
//   handler: function(req, res) {
//     console.log('pgsmall');
//   },
//   options: {}
// });
// TrieRouter.add({
//   methods: 'get',
//   path: '/{identity(page)}/{id([0-9a-f]{24}|\\w{3,30})}/{nav}',
//   handler: function(req, res) {
//     console.log('pgmed');
//   },
//   options: {}
// });
// TrieRouter.add({
//   methods: 'get',
//   path: '/{identity(page)}/{id([0-9a-f]{24}|\\w{3,30})}/{nav(posts)}/{tab(new)}',
//   handler: function(req, res) {
//     console.log('pglong');
//   },
//   options: {}
// });
//
//
// //TrieRouter.print();
// console.log('-----')
//
// var hrstart = 0;
// var hrend = 0;
// var ns = 9999999;
// var min = 9999999;
// var max  = 0;
// var total = 0;
//
// var itersb2 = 8;
// var iters = (1 << itersb2)// + 5;
//
// hrstart = process.hrtime();
// for (var i=0; i < iters; i++) {
//   //  TrieRouter.handle({method: 'GET', url: '/'}, {});
//   //  TrieRouter.handle({method: 'GET', url: '/test'}, {});
//   //  TrieRouter.handle({method: 'GET', url: '/hello/world'}, {});
//   // TrieRouter.handle({method: 'get', url: '/'}, {});
//   // TrieRouter.handle({method: 'get', url: '/about'}, {});
//   // TrieRouter.handle({method: 'get', url: '/page/taylorswift'}, {});
//   // TrieRouter.handle({method: 'get', url: '/page/taylorswift/posts'}, {});
//   // TrieRouter.handle({method: 'get', url: '/page/taylorswift/posts/new'}, {});
//   // TrieRouter.handle({method: 'get', url: '/something'}, {});
//   // TrieRouter.handle({method: 'get', url: '/this/that'}, {});
//   //mine.handle('get', '/page/taylorswift/posts/new');
//   mine.handle('get', '/lets/see/how/great/this/is');
// }
// hrend = process.hrtime(hrstart);
//
// ns = hrend[1];
// console.info("Execution time: %dns", ns);
// console.info("estimated average: %dns", ns / iters);
//
// console.log('END');
