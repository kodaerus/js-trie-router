'use strict'

function fakeGet() {
  //console.log('faked get request called');
}
function fakePost() {
  //console.log('faked post request called');
}

// trie
//  + root: node
function Trie(root) {
  this.root = root;
}

// node
//  + parent: node
//  + chCode: integer
//  + str: string
//  + children: [node]
//  + handlers: [method: fn] ?
function Node(parent, str, getFn, postFn) {
  this.parent = parent;
  this.str = str;
  this.chCode = str.charCodeAt(0);
  this.children = [];
  this.handlers = {
    get: getFn ? [getFn] : [],
    post: postFn ? [postFn] : []
  };
}

// returns a trie obj
function readTrie(done) {
  // reading in the trie
  //
  // create a root node (it's effectively '/')
  // set curNode to the root
  // set lastNode to null
  // set lastIndent to 2
  //
  // while have-lines-left:
  //   load a line
  //   if empty, skip
  //
  //   determine amount of indentation at beginning
  //   if line-indent equal to lastIndent:
  //     create a new node as a child of curNode, set lastNode to the new node
  //   if line-indent less than lastIndent:
  //     set curNode to curNode.parent, maybe error?
  //     create a new node as a child of curNode, set lastNode to the new node
  //   if line-indent greater than lastIndent:
  //     set curNode to lastNode
  //     create a new node as a child of curNode, set lastNode to the new node

  var trie = null;

  var curNode = null;
  var lastLevel = 0;
  var lastNode = null;

  var rl = require('readline').createInterface({
    input: require('fs').createReadStream('trie-routes.txt')
  });

  rl.on('line', function(line) {
    var trimmed = line.trim();
    if (!trimmed.length) {
      // skip the line
      return;
    }
    //console.log('readline:', trimmed);

    var indent = 0;
    for (var i = 0; i < line.length; i++) {
      if (line[i] === ' ') {
        indent++;
      } else {
        break;
      }
    }
    var level = indent / 2;

    var parts = trimmed.split('|');
    var str = parts[0];
    var options = parts[1];

    var getFn = null;
    var postFn = null;

    if (options) {
      if (options === 'g' || options === 'gp') {
        getFn = fakeGet;
      }
      if (options === 'p' || options === 'gp') {
        postFn = fakePost;
      }
    }

    if (level < lastLevel) {
      //console.log('going down in indent, go to parent');
      while (level < lastLevel) {
        curNode = curNode ? curNode.parent : null;
        lastLevel--;
      }
    } else if (level > lastLevel) {
      //console.log('going up in indent, must go deeper');
      curNode = lastNode;
    }

    lastLevel = level;

    lastNode = new Node(curNode, str, getFn, postFn);
    if (!curNode) {
      //console.log('create a new trie!');
      trie = new Trie(lastNode);
    } else {
      curNode.children.push(lastNode);
    }
  });

  rl.on('close', function() {
    done(null, trie);
  });
}


// assumes arr is integers formed by charCodeAt of the first char in the string
// chCode must also be obtained through chatCodeAt to be an int
function binarySearch(cArr, chCode) {
  var lb = 0;
  var ub = cArr.length - 1;
  var mid = 0;
  var node = null;
  var d = 0;
  var diff = 0;

  // console.log(cArr.length);
  while (true) {
    d = ub - lb;
    if (d < 0) {
      break;
    }
    mid = (d >> 1) + lb;
    // console.log('l', lb);
    // console.log('m', mid);
    // console.log('u', ub);
    node = cArr[mid];
    diff = node.chCode - chCode;

    // console.log('c', cArr[mid].chCode);
    // console.log('s', chCode);
    // console.log('d', diff);

    if (diff === 0) {
      return node;
    } else if (diff < 0) {
      lb = mid + 1;
    } else {
      ub = mid - 1;
    }
  }

  return null;
}

// returns node if a match, null if not
function searchTrie(trie, searchFor, method) {
  if (!trie || !trie.root || !trie.root.children || !trie.root.children.length) {
    console.log('no trie or root-with-children!');
    return null;
  }

  if (searchFor === '/') {
    return trie.root.handlers[method];
  }

  var i = 1; // current searchFor index
  var nexti = 0;
  var strLength = searchFor.length;

  var curNode = trie.root;
  var curChildren = curNode.children;
  var matchStr = null;

  // look for a match using the binarySearch on the i'th char
  // if there is one:
  //    validate the remainder of the potential match is contained within the searchFor
  //    if searchFor has no more && node has a handler: execute (finished)
  //    if searchFor has no more && node has no handler: 404-not-found (finished)
  //    if searchFor has more && node has no children: 404-not-found (finished)
  //    if searchFor has more && node has more children: set curNode to the matched-node, advance i by node.str.length, continue search

  while (curNode && i < strLength) {
    curNode = binarySearch(curChildren, searchFor.charCodeAt(i));

    // do we have a possible match?
    if (!curNode) {
      console.log('404 - not found (no first str match)');
      break;
    }

    matchStr = curNode.str;
    nexti = i + matchStr.length;

    // validate the rest of the match
    if (nexti > strLength || matchStr !== searchFor.substring(i, nexti)) {
      console.log('404 - not found (not full match)');
      break;
    }

    // advance i
    i = nexti;

    // still have more
    if (i < strLength) {
      curChildren = curNode.children;

      if (curChildren.length) {
        continue;
      } else {
        console.log('404 - not found (no more routes)');
        break;
      }
    }
    // no more left
    else {
      return curNode.handlers[method];
    }
  }

  return null;
}

function trieWalker(node, stack, count) {
  stack.push(node.str);

  var routeString = stack.join('');
  if (node.handlers) {
    if (node.handlers.get && node.handlers.get.length) {
      count++;
      console.log("get '" + routeString + "'");
    }
    if (node.handlers.post && node.handlers.post.length) {
      count++;
      console.log("post '" + routeString + "'");
    }
  }
  if (node.children && node.children.length) {
    for (var i=0; i < node.children.length; i++) {
      count += trieWalker(node.children[i], stack, 0);
    }
  }
  stack.pop();
  return count;
}

readTrie(function(error, trie) {
  // var count = trieWalker(trie.root, [], 0);
  // console.log('total num of routes: ', count);

  var route = '/verify';
  var method = 'get';
  // var route = '/account/company/cancelAvatar';
  // var method = 'post';

  var hrstart;
  var hrend;
  var test;

  var ns = 0;
  var min = 9999999;
  var max = 0;
  var total = 0;
  var avg = 0;

  var itersb2 = 8;
  var iters = (1 << itersb2) + 5;

  for (var i=0; i < iters; i++) {
    hrstart = process.hrtime();
    test = searchTrie(trie, route, method);
    hrend = process.hrtime(hrstart);
    if (test) {
      test[0]();
    } else {
      console.log('404 - no handler for route');
    }

    ns = hrend[1];
    if (i >= 5) {
      if (ns < min) {
        min = ns;
      }
      if (ns > max) {
        max = ns;
      }
      total += ns;
    }
    //console.info("Execution time: %dns", ns);
  }

  avg = total >> itersb2;

  console.info("Ran %d iterations", iters);
  console.info("Execution time: %dns", total);
  console.info("min: %dns", min);
  console.info("max: %dns", max);
  console.info("avg: %dns", avg);

  console.log('END');
});
